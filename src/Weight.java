

/**
 * 
 * @author Narut Poovorakit
 * @version 03.03.2015 An enum class of weight.
 *
 */
public enum Weight implements Unit {
	//US Unit
	HUNDREDWEIGHT("HUNDREDWEIGHT", 50.80234544),TON("TON", 1000),
	//Metric Unit
	KILOGRAM("KILOGRAM", 1.0), POUND("POUND", 0.45359237), 
	GRAM("GRAM", 0.001),
	KEED("KEED", 0.1 );

	private final String name;
	private final double value;

	/**
	 * A constructor of a weight.
	 * 
	 * @param name
	 *            is a name of the unit.
	 * @param value
	 *            is a value of a unit.
	 */
	private Weight(String name, double value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert a unit.
	 */
	public double convertTo(double amt, Unit unit) {
		return amt * value / unit.getValue();
	}

	/**
	 * @return the value.
	 */
	public double getValue() {
		return this.value;
	}

	/**
	 * @return the name.
	 */
	public String toString() {
		return this.name;
	}

}
