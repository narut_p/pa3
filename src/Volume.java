

/**
 * 
 * @author Narut Poovorakit
 * @version 03.03.2015 An enum class of weight.
 *
 */
public enum Volume implements Unit {
	//US Unit
	BARREL("BARREL", 6.28981),QUART("QUART",908.082 ),
	//Metric Unit
	CUBICMETER("CUBICMETER", 1.0), MILLILITER("MILLILITERS", 1000000), 
	LITER("LITER", 0.001),
	TANG("TANG", 0.02);

	private final String name;
	private final double value;

	/**
	 * A constructor of a volume.
	 * 
	 * @param name
	 *            is a name of the unit.
	 * @param value
	 *            is a value of a unit.
	 */
	private Volume(String name, double value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert a unit.
	 */
	public double convertTo(double amt, Unit unit) {
		return amt * value / unit.getValue();
	}

	/**
	 * @return the value.
	 */
	public double getValue() {
		return this.value;
	}

	/**
	 * @return the name.
	 */
	public String toString() {
		return this.name;
	}

}
