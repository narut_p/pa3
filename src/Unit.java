
/**
 * An interface of unit.
 * @author Narut Poovorakit
 * @version 09.03.2015
 *
 */
public interface Unit {

	public double convertTo(double amt, Unit unit);

	public double getValue();

	public String toString();
}
