

/**
 * 
 * @author Narut Poovorakit
 * @version 03.03.2015
 *
 */
public class UnitConverter {

	/**
	 * 
	 * @param amount
	 *            that the user input.
	 * @param fromUnit
	 *            is the first unit.
	 * @param toUnit
	 *            is the second unit.
	 * @return the amount that already convert.
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit) {
		return fromUnit.convertTo(amount, toUnit);
	}

	/**
	 * 
	 * @return all of value of the unit.
	 */
	public Unit[] getUnits(UnitType unit) {
		return unit.unitEnum;
		
	}
}
