
/**
 * 
 * @author Narut Poovorakit
 * @version 03.03.2015 An enum class of length.For instance, meter, mile and
 *          centermiter. �
 */
public enum Length implements Unit {
	METER("METER", 1.0), MILE("MILE", 1609.344), CENTIMETER("CENTIMETER", 0.01), FOOT(
			"FOOT", 0.30480), KILOMETER("KILOMETER", 1000.0), WA("WA", 2.0), NET(
			"Net's  unit", 1234567);

	private final String name;
	private final double value;

	/**
	 * A constructor of a length.
	 * 
	 * @param name
	 *            is a name of the unit.
	 * @param value
	 *            is a value of a unit.
	 */
	private Length(String name, double value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert a unit.
	 */
	public double convertTo(double amt, Unit unit) {
		return amt * value / unit.getValue();
	}

	/**
	 * @return the value.
	 */
	public double getValue() {
		return this.value;
	}

	/**
	 * @return the name.
	 */
	public String toString() {
		return this.name;
	}

}
