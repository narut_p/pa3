/**
 * 
 * @author Narut Poovorakit
 * @version 09.03.2015
 * An enum that contain a unit type.
 *
 */
public enum UnitType {
	LENGTH(Length.values()), AREA(Area.values()), WEIGHT(Weight.values()), VOLUME(
			Volume.values());

	public final Unit[] unitEnum;

	//initialize constructor.
	UnitType(Unit[] unit) {
		this.unitEnum = unit;
	}
}
