
/**
 * 
 * @author Narut Poovorakit
 * @version 03.03.2015 An enum class of area.
 *
 */
public enum Area implements Unit {
	// US Units
	ACRE("ACRE", 4046.8564), HECTARE("HECTARE", 10000),
	// METRIC Units
	SQUARECENTIMETER("SQUARECENTIMETER", 0.0001), SQUAREMETER("SQUAREMETER", 1), TOWNSHIP(
			"TOWNSHIP", 93239571.9721), RAI("RAI", 1600);

	private final String name;
	private final double value;

	/**
	 * A constructor of an area.
	 * 
	 * @param name
	 *            is a name of the unit.
	 * @param value
	 *            is a value of a unit.
	 */
	private Area(String name, double value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert a unit.
	 */
	public double convertTo(double amt, Unit unit) {
		return amt * value / unit.getValue();
	}

	/**
	 * @return the value.
	 */
	public double getValue() {
		return this.value;
	}

	/**
	 * @return the name.
	 */
	public String toString() {
		return this.name;
	}

}
