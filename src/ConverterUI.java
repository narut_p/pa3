import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ListDataListener;

/**
 * A graphical user interface of an unit converter.
 * 
 * @author Narut Poovorakit
 * @version 03.03.2015
 *
 */
public class ConverterUI extends JFrame {
	private JButton convertButton, clearButton;
	private JComboBox<Unit> comboBox1, comboBox2;
	private JTextField inputField, resultField;
	private JLabel labelEqual;
	private JMenu menu;
	private JMenuBar menuBar;
	private JMenuItem lengthItem, weightItem, volumeItem, areaItem;
	private UnitConverter uc;

	/**
	 * A constructor of converter GUI.
	 * 
	 * @param uc
	 *            is an object unit converter.
	 */
	public ConverterUI(UnitConverter uc) {
		super.setTitle("Length Converter");
		this.uc = uc;
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);

		initialize();
		super.setVisible(true);
	}

	/**
	 * Create a GUI component.
	 */
	public void initialize() {
		Container contents = this.getContentPane();
		contents.setLayout(new FlowLayout());

		// create button
		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");
		// create text field
		inputField = new JTextField(12);
		resultField = new JTextField(12);
		resultField.setEditable(false);
		// create combo box

		Unit[] length = uc.getUnits(UnitType.LENGTH);
		comboBox1 = new JComboBox<Unit>(length);
		comboBox2 = new JComboBox<Unit>(length);

		// create label
		labelEqual = new JLabel("=");

		contents.add(inputField);
		contents.add(comboBox1);
		contents.add(labelEqual);
		contents.add(resultField);
		contents.add(comboBox2);
		contents.add(convertButton);
		contents.add(clearButton);

		// Menu bar
		menuBar = new JMenuBar();
		menu = new JMenu("Unit Type");

		lengthItem = new JMenuItem("length");
		areaItem = new JMenuItem("area");
		volumeItem = new JMenuItem("volume");
		weightItem = new JMenuItem("weight");

		menu.add(lengthItem);
		menu.add(volumeItem);
		menu.add(areaItem);
		menu.add(weightItem);
		menu.addSeparator();
		menu.add(new ExitAction());

		menuBar.add(menu);

		super.setJMenuBar(menuBar);

		// button listener
		convertButton.addActionListener(new ConvertButtonListener());
		clearButton.addActionListener(new ClearButtonListener());
		inputField.addActionListener(new ConvertButtonListener());

		// Menu item listener
		volumeItem.addActionListener(new VolumeListener());
		lengthItem.addActionListener(new LengthListener());
		areaItem.addActionListener(new AreaListener());
		weightItem.addActionListener(new WeightListener());
		this.pack();

	}

	/**
	 * 
	 * An action when click or enter the button. It will calculate.
	 */
	class ConvertButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent evt) {
			try {
				resultField.setText(String.valueOf(uc.convert(
						Double.valueOf(inputField.getText().trim()),
						(Unit) comboBox1.getSelectedItem(),
						(Unit) comboBox2.getSelectedItem())));
				inputField.setForeground(Color.BLACK);
			} catch (Exception e) {
				inputField.setForeground(Color.RED);

			}
		}
	}

	/**
	 * 
	 * A action that perform when click a button. It will clear the text.
	 */
	class ClearButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			inputField.setText("");
			resultField.setText("");
			inputField.setForeground(Color.BLACK);
		}

	}

	/**
	 * When click it will exit.
	 */
	class ExitAction extends AbstractAction {
		public ExitAction() {
			super("Exit");
		}

		public void actionPerformed(ActionEvent evt) {
			System.exit(0);
		}
	}

	/**
	 * 
	 * Click volume.
	 */
	class VolumeListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			comboBox1.setModel(new DefaultComboBoxModel<Unit>(uc
					.getUnits(UnitType.VOLUME)));
			comboBox2.setModel(new DefaultComboBoxModel<Unit>(uc
					.getUnits(UnitType.VOLUME)));

		}
	}

	/**
	 * 
	 * Click length.
	 */
	class LengthListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			comboBox1.setModel(new DefaultComboBoxModel<Unit>(uc
					.getUnits(UnitType.LENGTH)));
			comboBox2.setModel(new DefaultComboBoxModel<Unit>(uc
					.getUnits(UnitType.LENGTH)));

		}
	}

	/**
	 * 
	 * Click area.
	 */
	class AreaListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			comboBox1.setModel(new DefaultComboBoxModel<Unit>(uc
					.getUnits(UnitType.AREA)));
			comboBox2.setModel(new DefaultComboBoxModel<Unit>(uc
					.getUnits(UnitType.AREA)));

		}
	}

	/**
	 * 
	 * Click weight.
	 */
	class WeightListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			comboBox1.setModel(new DefaultComboBoxModel<Unit>(uc
					.getUnits(UnitType.WEIGHT)));
			comboBox2.setModel(new DefaultComboBoxModel<Unit>(uc
					.getUnits(UnitType.WEIGHT)));

		}
	}
}
